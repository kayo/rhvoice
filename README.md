# Overview

This is a [RHVoise](https://github.com/olga-yakovleva/rhvoice) packaged for [Debian](http://debian.org/) by Kayo.

See also original README by Olga Yakovleva.

# Packages

+------------------------+-----------------------+----------------------+
| Package                | Internal dependencies | Descriptio           |
+------------------------+-----------------------+----------------------+
| librhvoice0            |                       | Main library         |
| librhvoice-dev         | librhvoice0           | Development files    |
| rhvoice                | librhvoice0           | Main executable      |
| rhvoice-client         |                       | Client for rhvoice   |
| rhvoice-data-elena     | librhvoice0           | Elena voice data     |
| rhvoice-data-aleksandr | librhvoice0           | Aleskandr voice data |
+------------------------+-----------------------+----------------------+
